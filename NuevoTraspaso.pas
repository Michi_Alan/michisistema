unit NuevoTraspaso;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.DateTimeCtrls, FMX.ListBox, FMX.Objects,
  FMX.Controls.Presentation;

type
  TFrame7 = class(TFrame)
    pan_Fondo: TPanel;
    rect_Contenido: TRectangle;
    lbl_NuevoTraspaso: TLabel;
    Line1: TLine;
    lbl_AlmOrigen: TLabel;
    cb_AlmOrigen: TComboBox;
    lbl_Fecha: TLabel;
    DateEdit1: TDateEdit;
    lbl_ProductoOrigen: TLabel;
    cb_ProductoOrigen: TComboBox;
    lbl_CantidadOrigen: TLabel;
    edt_CantidadOrigen: TEdit;
    lbl_Origen: TLabel;
    lbl_Destino: TLabel;
    cb_AlmacenDestino: TComboBox;
    lbl_AlmDestino: TLabel;
    lbl_FechaDestino: TLabel;
    DateEdit2: TDateEdit;
    lbl_ProductoDestino: TLabel;
    cb_ProductoDestino: TComboBox;
    lbl_CantidadDestino: TLabel;
    edt_CantidadDestino: TEdit;
    CornerButton1: TCornerButton;
    CornerButton2: TCornerButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}



end.
