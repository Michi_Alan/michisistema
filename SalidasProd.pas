unit SalidasProd;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.ListBox,
  FMX.DateTimeCtrls, FMX.Edit, FMX.Objects, FMX.Controls.Presentation;

type
  TFrame_Salida = class(TFrame)
    rect_container: TRectangle;
    lbl_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    edt_alm: TEdit;
    lbl_Alm: TLabel;
    lbl_fecha: TLabel;
    dateedt_fecha: TDateEdit;
    lbl_Obser: TLabel;
    edt_obser: TEdit;
    lbl_client: TLabel;
    combo_client: TComboBox;
    grd_salidas: TGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    btn_acept: TCornerButton;
    btn_cancel: TCornerButton;
    rec_botones: TRectangle;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}


end.
