program Project3;

uses
  System.StartUpCopy,
  FMX.Forms,
  Layout in 'Layout.pas' {Form8},
  Formularios in 'Formularios.pas' {Frame9: TFrame},
  Tablas in 'Tablas.pas' {Frame10: TFrame},
  LoginForm in 'LoginForm.pas' {Frame1: TFrame},
  Wcrypt2 in 'Imports\Wcrypt2.pas',
  Almacenes in 'Almacenes.pas' {Frame2: TFrame},
  RegistroAlm in 'RegistroAlm.pas' {Frame3: TFrame},
  DataModule in 'DataModule.pas' {CONN: TDataModule},
  Proveedor in 'Proveedor.pas' {Frame_proveedor: TFrame},
  IndexProveedor in 'IndexProveedor.pas' {Fam_indexprovedor: TFrame},
  RegistroUsuarios in 'RegistroUsuarios.pas' {registro_usuarios: TFrame},
  IndexArticulos in 'IndexArticulos.pas' {Frame_articulo: TFrame},
  Articulos in 'Articulos.pas' {Frame_articulos: TFrame},
  Insert_cte_shido in 'Insert_cte_shido.pas' {Frame_Cliente: TFrame},
  Clientes in 'Clientes.pas' {Frame_IndexCliente: TFrame},
  IndexEmpresa in 'IndexEmpresa.pas' {Frame_Empresa: TFrame},
  RegistroEmp in 'RegistroEmp.pas' {Frame_RegistroEmpresa: TFrame},
  SalidasProd in 'SalidasProd.pas' {Frame_Salida: TFrame},
  EntradasProd in 'EntradasProd.pas' {Frame6: TFrame},
  Traspasos in 'Traspasos.pas' {Frame5: TFrame},
  NuevoTraspaso in 'NuevoTraspaso.pas' {Frame7: TFrame},
  RegistroSuc in 'RegistroSuc.pas' {frm_registrosuc: TFrame},
  IndexSucursales in 'IndexSucursales.pas' {Frame_Sucursal: TFrame},
  IndexPaises in 'IndexPaises.pas' {Frame_Paises: TFrame},
  RegistroPais in 'RegistroPais.pas' {Frame_RegPais: TFrame},
  michiUtils in 'michiUtils.pas',
  RegistroGrupos in 'RegistroGrupos.pas' {registro_grupos: TFrame},
  IndexGrupos in 'IndexGrupos.pas' {index_grupos: TFrame},
  ModificarGruposUsuarios in 'ModificarGruposUsuarios.pas' {modificar_grupos_usuarios: TFrame},
  IndexEstados in 'IndexEstados.pas' {frm_IndexEstado: TFrame},
  RegistroEstado in 'RegistroEstado.pas' {Frame_RegistroEstado: TFrame},
  IndexMunicipios in 'IndexMunicipios.pas' {frm_IndexMunicipio: TFrame},
  RegistroMuni in 'RegistroMuni.pas' {frm_registroMuni: TFrame},
  RegistroDivisas in 'RegistroDivisas.pas' {Frame8: TFrame},
  IndexDivisas in 'IndexDivisas.pas' {FrameDivisas: TFrame},
  IndexCategoriaProducto in 'IndexCategoriaProducto.pas' {Index_CategoriaProductos: TFrame},
  RegistrarCategorias in 'RegistrarCategorias.pas' {Reg_categoriasProducto: TFrame},
  IndexMedidas in 'IndexMedidas.pas' {frm_indexMedidas: TFrame},
  RegistroMedidas in 'RegistroMedidas.pas' {frm_RegistroMedidas: TFrame},
  Rutas in 'Rutas.pas' {Frame4: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TCONN, CONN);
  Application.Run;
end.
