unit Almacenes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Controls.Presentation, System.Rtti, FMX.Grid.Style, FMX.Grid,
  FMX.ScrollBox, FMX.Edit, RegistroAlm, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope;

type
  TFrame2 = class(TFrame)
    rec_Bg_Container: TRectangle;
    �: TPanel;
    Line1: TLine;
    lbs_Titulo_Almacen: TLabel;
    btn_nuevo: TCornerButton;
    edt_almbuscar: TEdit;
    lbs_busqueda: TLabel;
    StringGrid1: TStringGrid;
    btn_mod: TCornerButton;
    btn_desactivar: TCornerButton;
    btn_buscar: TButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    RegistroAlm: TFrame3;
    procedure btn_nuevoClick(Sender: TObject);
    procedure btn_buscarClick(Sender: TObject);
    procedure RegistroAlmbtn_CancelarClick(Sender: TObject);
    procedure RegistroAlmbtn_GuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses Datamodule;
procedure TFrame2.btn_buscarClick(Sender: TObject);
begin
  Datamodule.CONN.SQL_SELECT.Close;
  Datamodule.CONN.SQL_SELECT.SQL.Text := 'SELECT ID,nombre,id_empresa,id_municipio ' +
                                          'FROM "almacenes" ' +
                                           'WHERE nombre = ''' + edt_almbuscar.Text + '''' ;
  Datamodule.CONN.SQL_SELECT.Open;
end;

procedure TFrame2.btn_nuevoClick(Sender: TObject);
begin
RegistroAlm.Visible := True;
RegistroAlm.cargardatos('123');
end;

procedure TFrame2.RegistroAlmbtn_CancelarClick(Sender: TObject);
begin
  RegistroAlm.Visible:= False;
  RegistroAlm.limpiardatos;
end;

procedure TFrame2.RegistroAlmbtn_GuardarClick(Sender: TObject);
var
  id_estado, id_municipio: integer;
  nombre, direc, col, cp: String;
begin
  {
    Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
    si este fuera el caso podra la variable como vacia y no podr� pasar la
    siguiente condici�n.
  }

    id_estado := RegistroAlm.cb_Estados.Selected.Index+1;
    id_municipio := RegistroAlm.cb_Municipio.Selected.Index+1;
    nombre := RegistroAlm.edt_Nombre.Text;
    direc := RegistroAlm.edt_Direccion.Text;
    col :=  RegistroAlm.edt_Colonia.text;
    cp := RegistroAlm.edt_CP.Text;

  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_estado = 0 ) and (id_municipio = 0) and (nombre = '') or (col = '') or (cp = '') or
      (direc = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO almacenes (nombre, direccion, colonia, CP, id_municipio, id_estado) VALUES (:nombre, :direc, :col, :cp, :muni, :estado)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('direc').AsString := direc;
    CONN.SQL_SELECT.ParamByName('col').AsString := col;
    CONN.SQL_SELECT.ParamByName('cp').AsString := cp;
    CONN.SQL_SELECT.ParamByName('muni').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      RegistroAlm.Visible:= False;
      //cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;


  end;
end;

end.
