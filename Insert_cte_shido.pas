unit Insert_cte_shido;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation, FMX.ListBox;

type
  TFrame_Cliente = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line2: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_agregar: TCornerButton;
    cb_Estados: TComboBox;
    cb_pais: TComboBox;
    edt_colonia: TEdit;
    edt_cp: TEdit;
    edt_direccion: TEdit;
    edt_nombre: TEdit;
    lbs_Colonia: TLabel;
    lbs_CP: TLabel;
    lbs_Direccion: TLabel;
    lbs_Estado: TLabel;
    lbs_Pais: TLabel;
    lbs_Nombre: TLabel;
    edt_telefono: TEdit;
    lbs_telefono: TLabel;
    lbs_cfdi: TLabel;
    cb_municipio: TComboBox;
    cb_tipo: TComboBox;
    edt_correo: TEdit;
    edt_rfc: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cb_cfdi: TComboBox;

    procedure limpiardatos;
    procedure cargardatos;
   // procedure cargardatos_municipio;
    procedure cb_EstadosClosePopup(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation
           uses      DataModule ;
{$R *.fmx}

procedure TFrame_Cliente.cb_EstadosClosePopup(Sender: TObject);
begin
  {
    Este evento se encargar� se cargar la informaci�n del municipio de acuerdo
    al estado seleccionado.

  cb_municipio.Items.Clear;
  cb_municipio.Enabled := True;


  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

  CONN.SQL_SELECT.ParamByName('id').AsInteger := cb_Estados.Selected.Index+1;
  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;

procedure TFrame_Cliente.limpiardatos;
begin
  //Limpiamos los edit.
  edt_colonia.Text := '';
  edt_cp.Text := '';
  edt_direccion.Text := '';
  edt_nombre.Text := '';
  edt_telefono.Text := '';
  edt_correo.Text := '';
  edt_rfc.Text := '';


  //Limpiamos los Combobox
  cb_Estados.Items.Clear;
  cb_municipio.Items.Clear;
  cb_Estados.Items.Clear;
   cb_cfdi.Items.Clear;
  end;



procedure TFrame_Cliente.cargardatos;
begin
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.Descripci�n AS "desc", a.c_UsoCFDI AS "CFDI" FROM f4_c_usocfdi a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_cfdi.Items.Add(CONN.SQL_SELECT.FieldByName('cfdi').AsString+ ' - ' + CONN.SQL_SELECT.FieldByName('desc').AsString);
    cb_cfdi.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;


  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM paises;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM estados WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Estados.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Estados.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    //cb_Estados.Items.Insert(CONN.SQL_SELECT.FieldByName('id').AsInteger, CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;


end.

