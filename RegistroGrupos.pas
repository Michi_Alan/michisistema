unit RegistroGrupos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.ListBox, FMX.Controls.Presentation, FMX.Edit;

type
  Tregistro_grupos = class(TFrame)
    Edit1: TEdit;
    lbl_name: TLabel;
    lb_available: TListBox;
    lb_selected: TListBox;
    btn_add: TButton;
    btn_remove: TButton;
    Panel1: TPanel;
    lbl_available: TLabel;
    lbl_selected: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

end.
