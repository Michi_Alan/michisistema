unit RegistroMuni;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, DataModule,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation;

type
  Tfrm_RegistroMuni = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    edt_Nombre: TEdit;
    lbs_Nombre: TLabel;
    edt_clave: TEdit;
    lbs_clave: TLabel;
    procedure cargardatos;
  private
    { Private declarations }
  public
    { Public declarations }
    var
     id: integer;
  end;

implementation

{$R *.fmx}
procedure Tfrm_RegistroMuni.cargardatos;
  begin
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.clave  FROM municipios a where a.id = :id;';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;
    CONN.SQL_SELECT.Open;



    if CONN.SQL_SELECT.RecordCount >0 then
    begin
      id:= CONN.SQL_SELECT.FieldByName('id').AsInteger;
      edt_Nombre.Text := CONN.SQL_SELECT.FieldByName('nombre').AsString;
      edt_clave.Text := CONN.SQL_SELECT.FieldByName('clave').AsString;

    end;
  end;

end.
