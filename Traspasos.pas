unit Traspasos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.ListBox,
  FMX.Objects, FMX.Controls.Presentation, NuevoTraspaso;

type
  TFrame5 = class(TFrame)
    pan_Fondo: TPanel;
    rec_Contenido: TRectangle;
    lbl_Traspasos: TLabel;
    Line1: TLine;
    lbl_Busqueda: TLabel;
    lbl_AlmOrigen: TLabel;
    cb_AlmOrigen: TComboBox;
    lbl_AlmDestino: TLabel;
    cb_AlmDestino: TComboBox;
    grd_Traspasos: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    btn_NuevoTraspaso: TCornerButton;
    Frame71: TFrame7;
    procedure btn_NuevoTraspasoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TFrame5.btn_NuevoTraspasoClick(Sender: TObject);
begin
Frame71.Visible := True;
end;

end.
