unit IndexMedidas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Edit, FMX.Objects,
  FMX.Controls.Presentation;

type
  TFrameMedidas = class(TFrame)
    insert_cont: TRectangle;
    Label1: TLabel;
    Line2: TLine;
    Panel1: TPanel;
    Rectangle1: TRectangle;
    CornerButton1: TCornerButton;
    CornerButton2: TCornerButton;
    edt_Nombre: TEdit;
    lbs_Nombre: TLabel;
    edt_mag: TEdit;
    lbs_mag: TLabel;
    lbs_ab: TLabel;
    tab_cont: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    grd_sucursales: TStringGrid;
    StringColumn1: TStringColumn;
    frm_RegistroMedidas1: Tfrm_RegistroMedidas;
    btn_agregar: TCornerButton;
    procedure cargartabla;
    procedure Grd_MedidasCellClick(const Column: TColumn; const Row: Integer);
    procedure Grd_MedidasCellDblClick(const Column: TColumn;
      const Row: Integer);
    procedure btn_configurarClick(Sender: TObject);
    procedure frm_RegistroMedidas1btn_configurarClick(Sender: TObject);
    procedure frm_RegistroMedidas1btn_desactivarClick(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}
procedure Tfrm_indexMedidas.btn_agregarClick(Sender: TObject);
begin
   frm_RegistroMedidas1.id := Self.id;
   frm_RegistroMedidas1.cargardatos;
   frm_RegistroMedidas1.Visible := True;
end;

procedure Tfrm_indexMedidas.btn_configurarClick(Sender: TObject);
begin
   frm_RegistroMedidas1.id := Self.id;
   frm_RegistroMedidas1.cargardatos;
   frm_RegistroMedidas1.Visible := True;
end;

procedure TFrameMedidas.btn_agregarClick(Sender: TObject);
begin
  tab_cont.Visible:=false;
  insert_cont.Visible:=true;
end;

procedure TFrameMedidas.CornerButton1Click(Sender: TObject);
begin

  abrev := frm_RegistroMedidas1.edt_abrev.Text;
  nombre := frm_RegistroMedidas1.edt_Nombre.Text;

  if (nombre = '') or (abrev = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO unidades_medidas (id, nombre, abrebiatura) VALUES (:id,:nombre, :abrev)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('abrev').AsString := abrev;
    CONN.SQL_SELECT.ParamByName('id').AsInteger := frm_RegistroMedidas1.id;

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      frm_RegistroMedidas1.Visible:= False;
      Self.cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;
  end;

end;

end.
