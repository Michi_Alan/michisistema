unit IndexEmpresa;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, FMX.Objects,
  FMX.Controls.Presentation, RegistroEmp;

type
  TFrame_Empresa = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    btn_agregar: TCornerButton;
    rec_botones: TRectangle;
    Grd_Empresa: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    StringColumn7: TStringColumn;
    frm_Registro: TFrame_RegistroEmpresa;
    StringColumn2: TStringColumn;
    procedure cargartabla;
    procedure btn_agregarClick(Sender: TObject);
    procedure frm_Registrobtn_configurarClick(Sender: TObject);
    procedure frm_Registrobtn_desactivarClick(Sender: TObject);
    procedure Grd_EmpresaCellClick(const Column: TColumn; const Row: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
                var
    I: integer;
implementation

{$R *.fmx}

uses DataModule;

procedure TFrame_Empresa.btn_agregarClick(Sender: TObject);
begin
  frm_Registro.Visible := True;
  frm_Registro.cargardatos;
end;

procedure TFrame_Empresa.cargartabla;

  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, b.c_RegimenFiscal AS ''cod'', b.`Descripci�n` AS ''desc'', a.razon_social, a.direccion, a.colonia, a.CP, a.telefono FROM empresa a JOIN f4_c_regimenfiscal b ON a.id_regimen = b.id;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Empresa.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_Empresa.Cells[0, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
          Grd_Empresa.Cells[1, I] := CONN.SQL_SELECT.FieldByName('cod').AsString + ' ' + CONN.SQL_SELECT.FieldByName('desc').AsString;
          Grd_Empresa.Cells[2, I] := CONN.SQL_SELECT.FieldByName('razon_social').AsString;
          Grd_Empresa.Cells[3, I] := CONN.SQL_SELECT.FieldByName('direccion').AsString;
          Grd_Empresa.Cells[4, I] := CONN.SQL_SELECT.FieldByName('colonia').AsString;
          Grd_Empresa.Cells[5, I] := CONN.SQL_SELECT.FieldByName('CP').AsString;
          Grd_Empresa.Cells[6, I] := CONN.SQL_SELECT.FieldByName('telefono').AsString;
          CONN.SQL_SELECT.Next;
        end;

    end;


  end;

procedure TFrame_Empresa.frm_Registrobtn_configurarClick(Sender: TObject);
begin
  frm_Registro.Visible := False;
  frm_Registro.limpiardatos;
  self.cargartabla;
end;

procedure TFrame_Empresa.frm_Registrobtn_desactivarClick(Sender: TObject);
var
  id_estado, id_regimen, id_municipio: integer;
  nombre, razon, rfc, tel, direc, col, cp: String;
begin
  {
    Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
    si este fuera el caso podra la variable como vacia y no podr� pasar la
    siguiente condici�n.
  }

    id_estado := frm_Registro.cb_Estados.Selected.Index+1;
    id_municipio := frm_Registro.cb_Municipio.Selected.Index+1;
    id_regimen := frm_Registro.cb_regimen.Selected.Index+1;
    nombre := frm_Registro.edt_Nombre.Text;
    razon :=  frm_Registro.edt_razonsocial.Text;
    rfc := frm_Registro.edt_rfc.Text;
    tel := frm_Registro.edt_telefono.Text;
    direc := frm_Registro.edt_Direccion.Text;
    col :=  frm_Registro.edt_Colonia.text;
    cp := frm_Registro.edt_CP.Text;

  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_estado = 0 ) and (id_municipio = 0) and (id_regimen = 0) or (nombre = '')
      or (razon = '') or (col = '') or (cp = '') or
      (direc = '') or (tel = '') or (rfc = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO empresa (nombre, id_regimen, razon_social, RFC, telefono, direccion, colonia, CP, id_municipio, id_estado) VALUES (:nombre, :regimen, :razon, :rfc, :tel, :direc, :col, :cp, :muni, :estado)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('regimen').AsString := id_regimen.ToString;
    CONN.SQL_SELECT.ParamByName('razon').AsString := razon;
    CONN.SQL_SELECT.ParamByName('rfc').AsString := rfc;
    CONN.SQL_SELECT.ParamByName('tel').AsString := tel;
    CONN.SQL_SELECT.ParamByName('direc').AsString := direc;
    CONN.SQL_SELECT.ParamByName('col').AsString := col;
    CONN.SQL_SELECT.ParamByName('cp').AsString := cp;
    CONN.SQL_SELECT.ParamByName('muni').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      frm_Registro.Visible:= False;
      cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;


  end;
end;

procedure TFrame_Empresa.Grd_EmpresaCellClick(const Column: TColumn;
  const Row: Integer);
begin
  ShowMessage(Row.ToString);
end;

end.
