unit IndexEstados;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,RegistroEstado,
  DataModule,FMX.Controls.Presentation;

type
  Tfrm_IndexEstado = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    Grd_Estados: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    Frame_RegistroEstado1: TFrame_RegistroEstado;
    procedure btn_configurarClick(Sender: TObject);
    procedure cargartabla;
    procedure Frame_RegistroEstado1btn_desactivarClick(Sender: TObject);
    procedure Grd_EstadosCellDblClick(const Column: TColumn; const Row: Integer);
    procedure Frame_RegistroEstado1btn_configurarClick(Sender: TObject);
    procedure Grd_EstadosCellClick(const Column: TColumn; const Row: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    var
      id: integer;
  end;

implementation

{$R *.fmx}

procedure Tfrm_IndexEstado.btn_configurarClick(Sender: TObject);
begin
  Frame_RegistroEstado1.id := Self.id;
  Frame_RegistroEstado1.cargardatos;
  Frame_RegistroEstado1.Visible := True;
end;

procedure Tfrm_IndexEstado.cargartabla;
  var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.abrev  FROM estados a;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Estados.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_Estados.Cells[0, I] := CONN.SQL_SELECT.FieldByName('abrev').AsString;
          Grd_Estados.Cells[1, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;


  end;

procedure Tfrm_IndexEstado.Frame_RegistroEstado1btn_configurarClick(
  Sender: TObject);
begin
  Frame_RegistroEstado1.Visible:= False;
  self.cargartabla;
end;

procedure Tfrm_IndexEstado.Frame_RegistroEstado1btn_desactivarClick(
  Sender: TObject);
var
abrev, nombre: String;
begin

  abrev := Frame_RegistroEstado1.edt_abrev.Text;
  nombre := Frame_RegistroEstado1.edt_Nombre.Text;

  if (nombre = '') or (abrev = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'UPDATE estados SET nombre= :nombre, abrev= :abrev WHERE id= :id';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('abrev').AsString := abrev;
    CONN.SQL_SELECT.ParamByName('id').AsInteger := Frame_RegistroEstado1.id;

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      Frame_RegistroEstado1.Visible:= False;
      Self.cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;
  end;


end;

procedure Tfrm_IndexEstado.Grd_EstadosCellClick(const Column: TColumn;
  const Row: Integer);
begin
  id :=  Row+1;
  btn_configurar.Enabled := True;
end;

procedure Tfrm_IndexEstado.Grd_EstadosCellDblClick(const Column: TColumn;
   const Row: Integer);
begin
  id :=  Row+1;
  btn_configurar.Enabled := True;
end;


end.
