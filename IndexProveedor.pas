unit IndexProveedor;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.Controls.Presentation;

type
  TFam_indexprovedor = class(TFrame)
    index_proveedor: TRectangle;
    Rectangle1: TRectangle;
    Line1: TLine;
    GridProveedor: TStringGrid;
    StringColumn1: TStringColumn;
    Rectangle2: TRectangle;
    btn_agregar: TCornerButton;
    Label1: TLabel;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    procedure GridProveedorCellClick(const Column: TColumn; const Row: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
      procedure cargartabla_proveedor;

  end;

implementation

{$R *.fmx}

uses DataModule, Proveedor;




procedure TFam_indexprovedor.cargartabla_proveedor;
   var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
   CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.rfc, a.telefono, a.direccion, a.colonia, a.correo FROM proveedores a';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          GridProveedor.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          GridProveedor.Cells[0, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
          GridProveedor.Cells[1, I] := CONN.SQL_SELECT.FieldByName('RFC').AsString;
          GridProveedor.Cells[2, I] := CONN.SQL_SELECT.FieldByName('telefono').AsString;
          GridProveedor.Cells[3, I] := CONN.SQL_SELECT.FieldByName('direccion').AsString;
          GridProveedor.Cells[4, I] := CONN.SQL_SELECT.FieldByName('colonia').AsString;
          GridProveedor.Cells[5, I] := CONN.SQL_SELECT.FieldByName('correo').AsString;
          //GridProveedor.Cells[6, I] := CONN.SQL_SELECT.FieldByName('telefono').AsString;
          CONN.SQL_SELECT.Next;
        end;

    end;


  end;

procedure TFam_indexprovedor.GridProveedorCellClick(const Column: TColumn;
  const Row: Integer);
  //para el cuadrado
begin
//mensaje de cada id de los campos
  //ShowMessage(Row.ToString);
end;

end.
