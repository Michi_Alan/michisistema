unit RegistroDivisas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.ListBox, FMX.Objects, FMX.Controls.Presentation;

type
  TFrame_RegistroDivisas = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    cb_Estados: TComboBox;
    cb_Municipio: TComboBox;
    edt_Colonia: TEdit;
    edt_CP: TEdit;
    edt_Direccion: TEdit;
    edt_Nombre: TEdit;
    lbs_Colonia: TLabel;
    lbs_CP: TLabel;
    lbs_Direccion: TLabel;
    lbs_Estado: TLabel;
    lbs_Municipio: TLabel;
    lbs_Nombre: TLabel;
    edt_telefono: TEdit;
    lbs_telefono: TLabel;
    lbs_regimen: TLabel;
    edt_rfc: TEdit;
    lbs_RFC: TLabel;
    edt_razonsocial: TEdit;
    lbs_razon: TLabel;
    cb_regimen: TComboBox;
    procedure cargardatos;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses DataModule;

procedure TFrame_RegistroDivisas.cargardatos;
begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM estados WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    //cb_Estados.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    //cb_Estados.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString, CONN.SQL_SELECT.FieldByName('id').AsString);
    cb_Estados.Items.Insert(CONN.SQL_SELECT.FieldByName('id').AsInteger, CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.Descripción AS "desc", a.c_RegimenFiscal AS "RF" FROM f4_c_regimenfiscal a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_regimen.Items.Add(CONN.SQL_SELECT.FieldByName('RF').AsString +' - '+ CONN.SQL_SELECT.FieldByName('desc').AsString);
    cb_regimen.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  //Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.

  cb_Municipio.Items.Add('');
  cb_Municipio.ItemIndex := 0;


end;
end.