﻿MichiSistema.


Abreviaturas:

Buttons: btn_
Edits: edt_
Frames: frm_
Rectangles: rec_
Labels: lbs_
ComboBox: cb_
Text: txt_
Panel: pan_
StringGrid: grd_

Animations: objeto animado más el sufijo "Animation"
Ejemplo, btn_loginAnimation.


Colores para los Botones (StylesLockups):

btn_lateral: Para todos los botones del menu lateral.
btn_login: Boton para el frame del login.
btn_guardar: Botón color azul
btn_cancelar: Botón color rojo
btn_modificar: Botón color amarillo


Tipografia:

Tipo de letra: Microsoft JhengHei
Tamaños: {Titulos: 20, Texto: 12}
Negritas

