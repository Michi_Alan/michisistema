unit RegistroSuc;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.ListBox, FMX.Objects, FMX.Controls.Presentation,DataModule;

type
  Tfrm_registrosuc = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    cb_Estados: TComboBox;
    cb_Municipio: TComboBox;
    edt_Colonia: TEdit;
    edt_CP: TEdit;
    edt_Direccion: TEdit;
    edt_Nombre: TEdit;
    lbs_Colonia: TLabel;
    lbs_CP: TLabel;
    lbs_Direccion: TLabel;
    lbs_Estado: TLabel;
    lbs_Municipio: TLabel;
    lbs_Nombre: TLabel;
    edt_telefono: TEdit;
    lbs_telefono: TLabel;
    edt_rfc: TEdit;
    lbs_RFC: TLabel;
    lbs_empresa: TLabel;
    lbs_Pais: TLabel;
    cb_Empresa: TComboBox;
    cb_Pais: TComboBox;
 procedure limpiardatos;
    procedure cargardatos;
    procedure cb_EstadosClosePopup(Sender: TObject);
    procedure btn_desactivarClick(Sender: TObject);
    procedure btn_configurarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}
procedure Tfrm_registrosuc.cb_EstadosClosePopup(Sender: TObject);
begin
  {
    Este evento se encargar� se cargar la informaci�n del municipio de acuerdo
    al estado seleccionado.

    @PARAMS
      /*id = Corresponde al id del estado seleccionado m�s 1 para que corresponda
           con el correcto.

    @NOTAS
      /* Siempre iniciar limpiando el combobox, esto corresponde a la funcion
        <<Items.Clear>>
      /* Hay que bloquear el combobox con el atributo Enabled, cuando seleccionemos
         un estado este se desbloquear�

  }

  cb_Municipio.Items.Clear;
  cb_Municipio.Enabled := True;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

  CONN.SQL_SELECT.ParamByName('id').AsInteger := cb_Estados.Selected.Index+1;
  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;

procedure Tfrm_registrosuc.limpiardatos;
begin
  //Limpiamos los edit.
  edt_Colonia.Text := '';
  edt_CP.Text := '';
  edt_Direccion.Text := '';
  edt_Nombre.Text := '';
  edt_telefono.Text := '';
  edt_rfc.Text := '';


  //Limpiamos los Combobox
  cb_Estados.Items.Clear;
  cb_Municipio.Items.Clear;
  cb_Estados.Items.Clear;
  cb_Empresa.Items.Clear;
end;


procedure Tfrm_registrosuc.btn_configurarClick(Sender: TObject);
begin
self.Visible := False;
end;

procedure Tfrm_registrosuc.btn_desactivarClick(Sender: TObject);
var
Nombre, RFC, Direccion,telefono, Colonia, CP, empresa, Municipio, Estado, Pais: String;
begin

  Nombre:= edt_Nombre.Text;
  RFC:= edt_RFC.Text;
  Direccion := edt_Direccion.Text;
  telefono:= edt_telefono.Text;
  Colonia := edt_Colonia.Text;
  CP := edt_CP.Text;

  if (Nombre = '') or (Direccion = '')or (telefono = '') or (Colonia = '') or (Municipio = '') or (Estado = '') or (CP = '') or (pais = '') or (RFC = '') or (empresa = '')then
    begin
      ShowMessage('�No puedes dejar informaci�n vacia!');
    end
  else
    begin
            CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'INSERT `sucursales` SET `nombre`=:Nombre,`RFC`=:RFC,`direccion`=:Direccion,`telefono`=:telefono,`colonia`=:Colonia, `CP`=:CP';

            CONN.SQL_SELECT.ParamByName('Nombre').AsString := Nombre;
            CONN.SQL_SELECT.ParamByName('RFC').AsString := RFC;
            CONN.SQL_SELECT.ParamByName('Direccion').AsString := Direccion;
            CONN.SQL_SELECT.ParamByName('Telefono').AsString := telefono;
            CONN.SQL_SELECT.ParamByName('Colonia').AsString := Colonia;
            CONN.SQL_SELECT.ParamByName('CP').AsString := CP;
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then
              begin
                ShowMessage('�Se han actualizado los datos!');
              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;
              end;
end;

procedure Tfrm_registrosuc.cargardatos;
begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM estados WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Estados.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Estados.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM empresa;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Empresa.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_empresa.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM paises;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;



  //Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.

  cb_Municipio.Items.Add('');
  cb_Municipio.ItemIndex := 0;
end;
end.
