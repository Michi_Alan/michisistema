unit Proveedor;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Objects, FMX.Edit, FMX.ListBox, FMX.Layouts,
  IndexProveedor, FireDAC.UI.Intf, FireDAC.FMXUI.Wait, FireDAC.Stan.Intf,
  FireDAC.Comp.UI, Data.Bind.EngExt, Fmx.Bind.DBEngExt, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.DBScope;

type
  TFrame_proveedor = class(TFrame)
    Fondo: TRectangle;
    Label2: TLabel;
    Edit_nombre: TEdit;
    Edit_direccion: TEdit;
    Edit_rfc: TEdit;
    Edit_cp: TEdit;
    Edit_telefono: TEdit;
    Edit_correo: TEdit;
    Edit_colonia: TEdit;
    Lab_Formu: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Select_empresa: TComboBox;
    Select_pais: TComboBox;
    Select_municipio: TComboBox;
    contenido: TRectangle;
    contenido1: TRectangle;
    Select_estado: TComboBox;
    btn_guardar: TCornerButton;
    btn_cancelar: TCornerButton;
    FD_datos: TFDGUIxWaitCursor;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    Line1: TLine;
    Rectangle1: TRectangle;
    Label12: TLabel;
    Select_pago: TComboBox;
    procedure btn_guardarClick(Sender: TObject);
    procedure btn_cancelarClick(Sender: TObject);
    procedure Select_estadoClosePopup(Sender: TObject);

  private

    { Private declarations }
  public
    { Public declarations }
    procedure  limpiar();
    //para empresa
    function cargar_empresa(nombre: string): string;
    //para Paises
    function cargar_pais(nombre: string): string;
    //para municipio
    //function cargar_municipio(nombre: string): string;
    //ejemplo de cargar por estados
//    procedure cargar_EstadosClosePopup(Sender: TObject);
    procedure cargardatos_estados;
    procedure cargardatos_municipio;
    function cargar_formapagos(nombre: string): string;
  end;

implementation

{$R *.fmx}

uses DataModule;



procedure TFrame_proveedor.cargardatos_estados;

begin
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM estados WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin

    Select_estado.Items.Insert(CONN.SQL_SELECT.FieldByName('id').AsInteger, CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;


////Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.
//
//  Select_municipio.Items.Add('');
//  Select_municipio.ItemIndex := 0;


end;
procedure TFrame_proveedor.cargardatos_municipio;
begin
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin

    Select_municipio.Items.Insert(CONN.SQL_SELECT.FieldByName('id').AsInteger, CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;


////Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.
//
//  Select_municipio.Items.Add('');
//  Select_municipio.ItemIndex := 0;


end;

function TFrame_proveedor.cargar_empresa(nombre: string): string;
begin

  {
  Esta funcion hace el llenado de los combobox, se necesita una consulta que debera traer los datos
  necesarios.

  }
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM empresa';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    Select_empresa.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    Select_empresa.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;

//procedure TFrame_proveedor.cargar_EstadosClosePopup(Sender: TObject);
//
//var
//text : String;
//begin
//
//  Select_municipio.Items.Clear;
//  Select_municipio.Enabled := True;
//  text:= Select_estado.Selected.Text;
//
//  ShowMessage(Select_estado.Items.IndexOf(text).ToString);
//
//  CONN.SQL_SELECT.Close;
//  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios';
//
//  CONN.SQL_SELECT.ParamByName('id').AsInteger := Select_estado.Selected.Index+1;
//  CONN.SQL_SELECT.Open;
//
//  while not CONN.SQL_SELECT.Eof do
//  begin
//    Select_municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
//    Select_municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
//    CONN.SQL_SELECT.Next;
//  end;
//
//end;

function TFrame_proveedor.cargar_formapagos(nombre: string): string;
begin

  {
  Para cargar los nombre de forma de pagos.

  }
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM tipo_pago';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    Select_pago.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    Select_pago.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
end;



//function TFrame_proveedor.cargar_municipio(nombre: string): string;
//begin
// {
//  Esta funcion hace el llenado de los combobox, se necesita una consulta que debera traer los datos
//  necesarios.
//
//  }
//  CONN.SQL_SELECT.Close;
//  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios';
//
//  CONN.SQL_SELECT.Open;
//
//  while not CONN.SQL_SELECT.Eof do
//  begin
//    Select_empresa.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
//    Select_empresa.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
//    CONN.SQL_SELECT.Next;
//  end;
//
//end;

function TFrame_proveedor.cargar_pais(nombre: string): string;
begin

  {
  Esta funcion hace el llenado de los combobox, se necesita una consulta que debera traer los datos
  necesarios.

  }
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM paises';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    Select_pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    Select_pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;

procedure TFrame_proveedor.btn_cancelarClick(Sender: TObject);
begin
limpiar;
end;

procedure TFrame_proveedor.btn_guardarClick(Sender: TObject);
var
  id_empresa,id_municipio,id_estado,id_pais,id_pago: integer;
  nombre, rfc, tel, direc, col,correo, CP: String;
begin

    id_empresa := Select_empresa.Selected.Index+1;
    id_estado := Select_estado.Selected.Index+1;
    id_municipio := Select_municipio.Selected.Index+1;
    id_pais := Select_pais.Selected.Index+1;
    id_pago := Select_pago.Selected.Index+1;
    nombre := Edit_Nombre.Text;
    rfc :=  Edit_rfc.Text;
    tel :=  Edit_telefono.Text;
    direc :=   Edit_direccion.Text;
    col :=     Edit_colonia.Text;
    correo :=   Edit_correo.Text;
    CP :=   Edit_cp.Text;

//  [ShowMessage(id_estado.ToString+', '+id_municipio.ToString+', '+id_empresa.ToString);
//  ]
  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_estado = 0 ) and (id_municipio = 0) and (id_empresa = 0) and (id_pago = 0) and (id_pais = 0) or (nombre = '')
      or (rfc = '') or (col = '') or (cp = '') or
      (direc = '') or (tel = '') or (rfc = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');

  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO proveedores (nombre, RFC, telefono, direccion, colonia, correo, CP, id_empresa, id_municipio, id_estado, id_pais, id_pago) VALUES (:nombre, :RFC, :telefono, :direccion, :colonia, :correo, :CP, :id_empresa, :id_municipio, :estado, :pais, :pago)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('RFC').AsString := rfc;
    CONN.SQL_SELECT.ParamByName('telefono').AsString := tel;
    CONN.SQL_SELECT.ParamByName('direccion').AsString := direc;
    CONN.SQL_SELECT.ParamByName('colonia').AsString := col;
    CONN.SQL_SELECT.ParamByName('correo').AsString := correo;
    CONN.SQL_SELECT.ParamByName('CP').AsString := CP;
    CONN.SQL_SELECT.ParamByName('id_empresa').AsString := id_empresa.ToString;
    CONN.SQL_SELECT.ParamByName('id_municipio').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
    CONN.SQL_SELECT.ParamByName('pais').AsString := id_pais.ToString;
    CONN.SQL_SELECT.ParamByName('pago').AsString := id_pago.ToString;

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado el proveedor!:  ' +Edit_nombre.Text);

    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;


  end;

end;

procedure TFrame_proveedor.limpiar;
begin
//Lipiar lo campos
Edit_nombre.Text := '';
Edit_direccion.Text := '';
Edit_telefono.Text := '';
Edit_colonia.Text := '';
Edit_rfc.Text := '';
Edit_correo.Text := '';
Edit_cp.Text := '';
Select_empresa.clear;
Select_estado.clear;
Select_municipio.clear;
Select_pais.clear;
Select_pago.clear;
end;

procedure TFrame_proveedor.Select_estadoClosePopup(Sender: TObject);
begin
  Select_municipio.Items.Clear;
  Select_municipio.Enabled := True;


  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

  CONN.SQL_SELECT.ParamByName('id').AsInteger := Select_estado.Selected.Index+1;
  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    Select_municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    Select_municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
end;

end.





