unit IndexSucursales;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, FMX.Objects, RegistroSuc,
  DataModule,FMX.Controls.Presentation;

type
  TFrame_Sucursal = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    grd_sucursales: TStringGrid;
    frm_registrosuc1: Tfrm_registrosuc;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    StringColumn7: TStringColumn;
    tab_cont: TRectangle;
    Label1: TLabel;
    Line2: TLine;
    Panel1: TPanel;
    Rectangle1: TRectangle;
    CornerButton1: TCornerButton;
    CornerButton2: TCornerButton;
    CornerButton3: TCornerButton;
    StringGrid1: TStringGrid;
    StringColumn8: TStringColumn;
    StringColumn9: TStringColumn;
    StringColumn10: TStringColumn;
    procedure btn_agregarClick(Sender: TObject);
    procedure cargartabla;
    procedure btn_configurarClick(Sender: TObject);
    procedure Grd_SucursalesCellClick(const Column: TColumn; const Row: Integer);
    procedure frm_registrosuc1btn_desactivarClick(Sender: TObject);
    procedure frm_registrosuc1btn_configurarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TFrame_Sucursal.btn_agregarClick(Sender: TObject);
begin
   frm_registrosuc1.Visible := True;
   frm_registrosuc1.cargardatos;
end;
procedure TFrame_Sucursal.cargartabla;
  var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.nombre, a.RFC, a.direccion, a.telefono, a.colonia, a.CP, a.id_empresa FROM sucursales a';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin

          grd_sucursales.Cells[0, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
          grd_sucursales.Cells[1, I] := CONN.SQL_SELECT.FieldByName('RFC').AsString;
          grd_sucursales.Cells[2, I] := CONN.SQL_SELECT.FieldByName('direccion').AsString;
          grd_sucursales.Cells[3, I] := CONN.SQL_SELECT.FieldByName('telefono').AsString;
          grd_sucursales.Cells[4, I] := CONN.SQL_SELECT.FieldByName('colonia').AsString;
          grd_sucursales.Cells[5, I] := CONN.SQL_SELECT.FieldByName('CP').AsString;
          grd_sucursales.Cells[6, I] := CONN.SQL_SELECT.FieldByName('id_empresa').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;


  end;

procedure TFrame_Sucursal.frm_registrosuc1btn_configurarClick(Sender: TObject);
begin
  frm_registrosuc1.Visible:= False;
  cargartabla;
end;

procedure TFrame_Sucursal.frm_registrosuc1btn_desactivarClick(Sender: TObject);
var
  id_empresa,id_estado, id_pais, id_municipio: integer;
  nombre,  rfc, tel, direc, col, cp: String;
begin
    {
      Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
      si este fuera el caso podra la variable como vacia y no podr� pasar la
      siguiente condici�n.
    }

      id_estado := frm_registrosuc1.cb_Estados.Selected.Index+1;
      id_municipio := frm_registrosuc1.cb_Municipio.Selected.Index+1;
      id_pais := frm_registrosuc1.cb_Empresa.Selected.Index+1;
      nombre := frm_registrosuc1.edt_Nombre.Text;
      id_empresa := frm_registrosuc1.cb_empresa.Selected.Index+1;
      rfc := frm_registrosuc1.edt_rfc.Text;
      tel := frm_registrosuc1.edt_telefono.Text;
      direc := frm_registrosuc1.edt_Direccion.Text;
      col :=  frm_registrosuc1.edt_Colonia.text;
      cp := frm_registrosuc1.edt_CP.Text;

    //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

    if (id_estado = 0 ) and (id_municipio = 0) and (id_pais = 0)and (id_empresa = 0) or (nombre = '')
         or (col = '') or (cp = '') or
        (direc = '') or (tel = '') or (rfc = '') then
    begin
      ShowMessage('�Los campos no pueden estar vacios!');
    end
    else
    begin

      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'INSERT INTO sucursales (nombre, RFC, direccion, telefono, colonia, CP, id_empresa, id_municipio, id_estado, id_pais) VALUES (:nombre, :rfc, :direc, :tel, :col, :cp, :empresa, :muni, :estado, :pais)';

      CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
      CONN.SQL_SELECT.ParamByName('rfc').AsString := rfc;
      CONN.SQL_SELECT.ParamByName('tel').AsString := tel;
      CONN.SQL_SELECT.ParamByName('direc').AsString := direc;
      CONN.SQL_SELECT.ParamByName('col').AsString := col;
      CONN.SQL_SELECT.ParamByName('cp').AsString := cp;
      CONN.SQL_SELECT.ParamByName('empresa').AsString := id_empresa.ToString;
      CONN.SQL_SELECT.ParamByName('muni').AsString := id_municipio.ToString;
      CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
      CONN.SQL_SELECT.ParamByName('pais').AsString := id_pais.ToString;
      CONN.SQL_SELECT.ExecSQL;

      if (CONN.SQL_SELECT.RowsAffected > 0) then
      begin
        ShowMessage('�Se ha registrado la informaci�n!');
        frm_registrosuc1.Visible:= False;
        cargartabla;
      end
      else
      begin
        ShowMessage('Algo ha salido mal...');
      end;

  end;
end;

procedure TFrame_Sucursal.btn_configurarClick(Sender: TObject);
begin
  frm_registrosuc1.Visible := False;
  frm_registrosuc1.limpiardatos;
  self.cargartabla;
end;

procedure TFrame_Sucursal.Grd_SucursalesCellClick(const Column: TColumn;
  const Row: Integer);
begin
  ShowMessage(Row.ToString);
end;


end.
