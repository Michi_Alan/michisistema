unit RegistroUsuarios;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation,System.RegularExpressions, Formularios, WCrypt2;

type
  Tregistro_usuarios = class(TFrame)
    Rectangle1: TRectangle;
    Label1: TLabel;
    Line1: TLine;
    Panel2: TPanel;
    lbl_usuario: TLabel;
    edt_usuario: TEdit;
    lbl_password: TLabel;
    edt_password: TEdit;
    lbl_correo: TLabel;
    edt_correo: TEdit;
    cornbtn_formu2: TCornerButton;
    cornbtn_formu1: TCornerButton;
    Image1: TImage;
    Image2: TImage;
    Image4: TImage;
    Image3: TImage;
    cb_resetpwd: TCheckBox;
    procedure cornbtn_formu1Click(Sender: TObject);
    function IsMatch(const Input, Pattern: string): boolean;
    function IsValidEmailRegEx(const EmailAddress: string): boolean;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses DataModule;
{$R *.fmx}

function md5(const Input: String): String;
var
  hCryptProvider: HCRYPTPROV;
  hHash: HCRYPTHASH;
  bHash: array[0..$7f] of Byte;
  dwHashBytes: Cardinal;
  pbContent: PByte;
  i: Integer;
begin
  dwHashBytes := 16;
  pbContent := Pointer(PChar(Input));
  Result := '';
  if CryptAcquireContext(@hCryptProvider, nil, nil, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT or CRYPT_MACHINE_KEYSET) then
  begin
    if CryptCreateHash(hCryptProvider, CALG_MD5, 0, 0, @hHash) then
    begin
      if CryptHashData(hHash, pbContent, Length(Input) * sizeof(Char), 0) then
      begin
        if CryptGetHashParam(hHash, HP_HASHVAL, @bHash[0], @dwHashBytes, 0) then
        begin
          for i := 0 to dwHashBytes - 1 do
          begin
            Result := Result + Format('%.2x', [bHash[i]]);
          end;
        end;
      end;
      CryptDestroyHash(hHash);
    end;
    CryptReleaseContext(hCryptProvider, 0);
  end;
  Result := AnsiLowerCase(Result);
end;




function Tregistro_usuarios.IsMatch(const Input, Pattern: string): boolean;
begin
  Result := TRegEx.IsMatch(Input, Pattern);
end;
function Tregistro_usuarios.IsValidEmailRegEx(const EmailAddress: string): boolean;
const
  EMAIL_REGEX = '^((?>[a-zA-Z\d!#$%&''*+\-/=?^_`{|}~]+\x20*|"((?=[\x01-\x7f])'
             +'[^"\\]|\\[\x01-\x7f])*"\x20*)*(?<angle><))?((?!\.)'
             +'(?>\.?[a-zA-Z\d!#$%&''*+\-/=?^_`{|}~]+)+|"((?=[\x01-\x7f])'
             +'[^"\\]|\\[\x01-\x7f])*")@(((?!-)[a-zA-Z\d\-]+(?<!-)\.)+[a-zA-Z]'
             +'{2,}|\[(((?(?<!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d))'
             +'{4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\'
             +'[\x01-\x7f])+)\])(?(angle)>)$';
begin
  Result := IsMatch(EmailAddress, EMAIL_REGEX);
end;

procedure Tregistro_usuarios.cornbtn_formu1Click(Sender: TObject);
var
Usuario, Password, Correo: String;
begin
  Usuario:= edt_usuario.Text;
  Password:= edt_password.Text;
  Correo:= edt_correo.Text;

  if (Usuario = '') or (Password = '') or (Correo = '') then
    begin
      ShowMessage('�No puedes dejar informaci�n vacia!');
    end
  else
    begin
      if IsValidEmailRegEx(Correo) then
      begin
        CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'INSERT `usuarios` SET `usuario`=:Usuario,`pass`=:Password,`correo`=:Correo';

            CONN.SQL_SELECT.ParamByName('Usuario').AsString := Usuario;
            CONN.SQL_SELECT.ParamByName('Password').AsString := md5(Password);
            CONN.SQL_SELECT.ParamByName('Correo').AsString := Correo;
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then

              begin
                ShowMessage('�Se han actualizado los datos!');
                edt_usuario.Text:='';
                edt_password.Text:='';
                edt_correo.Text:='';
              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;
      end
      else
        ShowMessage('Correo inv�lido');

    end;
end;

end.
