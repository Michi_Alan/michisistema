unit DataModule;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UniProvider, MySQLUniProvider,
  MemDS, DBAccess, Uni, Datasnap.DBClient, Datasnap.Provider;

type
  TCONN = class(TDataModule)
    SQL_CONNECTION: TUniConnection;
    SQL_SELECT: TUniQuery;
    MySQLUniProvider1: TMySQLUniProvider;
    UniQuery1: TUniQuery;
    ClientDataSet1: TClientDataSet;
    DataSetProvider1 : TDataSetProvider;
    procedure Nuevo;
    procedure Aplicar;
    procedure Modificar;
    procedure Eliminar;


  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  CONN: TCONN;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TCONN }

procedure TCONN.Aplicar;
begin
    ClientDataSet1.Post;    //Guardar los datos en RAM
    ClientDataSet1.ApplyUpdates(0);  // pasar los datos a la base
    ClientDataSet1.Close;
    ClientDataSet1.Open;
end;

procedure TCONN.Eliminar;
begin
   ClientDataSet1.Delete;    //Guardar los datos en RAM
    ClientDataSet1.ApplyUpdates(0);  // pasar los datos a la base
    ClientDataSet1.Close;
    ClientDataSet1.Open;
end;

procedure TCONN.Modificar;
begin
    ClientDataSet1.Edit;
end;

procedure TCONN.Nuevo;
begin
    ClientDataSet1.Insert;
end;

end.
