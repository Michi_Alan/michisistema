unit IndexIva;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.ListBox, FMX.Grid, FMX.ScrollBox, FMX.Edit,
  FMX.Objects, FMX.Controls.Presentation;

type
  TFrameIva = class(TFrame)
    insert_cont: TRectangle;
    Label1: TLabel;
    Line2: TLine;
    Panel1: TPanel;
    Rectangle1: TRectangle;
    CornerButton1: TCornerButton;
    CornerButton2: TCornerButton;
    edt_porcentaje: TEdit;
    lbs_porcentaje: TLabel;
    lbs_pais: TLabel;
    lbs_motivo: TLabel;
    edt_motivo: TEdit;
    tab_cont: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    grd_sucursales: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn4: TStringColumn;
    cb_Pais: TComboBox;
    procedure CornerButton1Click(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TFrameIva.btn_agregarClick(Sender: TObject);
begin
  insert_cont.Visible:=true;
  tab_cont.Visible:=false;
end;

procedure TFrameIva.CornerButton1Click(Sender: TObject);
begin
  insert_cont.Visible:=false;
  tab_cont.Visible:=true;
end;

end.
