﻿unit Layout;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.Ani,
  System.ImageList, FMX.ImgList, WCrypt2, LoginForm, Tablas, Formularios,
  Proveedor, IndexProveedor, Almacenes, RegistroUsuarios, IndexArticulos,
  Articulos, Clientes, IndexEmpresa, IndexSucursales, FMX.ListBox, FMX.Layouts,
  IndexPaises, IndexMunicipios, IndexEstados, RegistroEstado, IndexDivisas,
  IndexCategoriaProducto, IndexMedidas;

type
  TForm8 = class(TForm)
    Main_rec: TRectangle;
    top_rec: TRectangle;
    left_rec: TRectangle;
    client_rec: TRectangle;
    rec_btn: TButton;
    cerrar_rec: TFloatAnimation;
    abrir_rec: TFloatAnimation;
    StyleBook1: TStyleBook;
    ImageList1: TImageList;
    Label1: TLabel;
    btn_salir: TCornerButton;
    frm_login: TFrame1;
    frm_formularios: TFrame9;
    frm_tablas: TFrame10;
    btn_formularios: TCornerButton;
    btn_proveedor: TCornerButton;
    registro_usuarios1: Tregistro_usuarios;
    btn_articulos: TCornerButton;
    btn_cliente: TCornerButton;
    frm_articulo: TFrame_articulo;
    frm_almacen: TFrame2;
    frm_empresa: TFrame_Empresa;
    frm_sucursal: TFrame_Sucursal;
    Configuracion_list: TListBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem5: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    ListBoxItem7: TListBoxItem;
    frm_paises: TFrame_Paises;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    ListBoxItem8: TListBoxItem;
    ListBoxItem9: TListBoxItem;
    Fam_indexprovedor1: TFam_indexprovedor;
    frm_cliente: TFrame_IndexCliente;
    frm_Estados: Tfrm_IndexEstado;
    frm_Municipio: Tfrm_IndexMunicipio;
    Frame_proveedor1: TFrame_proveedor;
    CornerButton1: TCornerButton;
    Index_CategoriaProductos1: TIndex_CategoriaProductos;
    frm_div: TFrameDivisas;
    FrameMedidas1: TFrameMedidas;
    Medidas: TListBoxItem;
    procedure btn_salirClick(Sender: TObject);
    procedure frm_loginCornerButton1Click(Sender: TObject);
    procedure rec_btnClick(Sender: TObject);
    procedure btn_formulariosClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_proveedorClick(Sender: TObject);
    procedure Fam_indexprovedor1btn_agregarClick(Sender: TObject);
    procedure Frame_proveedor1btn_cancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Frame_proveedor1btn_guardarClick(Sender: TObject);
    procedure btn_clienteClick(Sender: TObject);
    procedure ListBoxItem1Click(Sender: TObject);
    procedure ListBoxItem2Click(Sender: TObject);
    procedure ListBoxItem3Click(Sender: TObject);
    procedure ListBoxItem4Click(Sender: TObject);
    procedure ListBoxItem5Click(Sender: TObject);
    procedure ListBoxItem6Click(Sender: TObject);
    procedure ListBoxItem7Click(Sender: TObject);
    procedure DivisasClick(Sender: TObject);
    procedure CornerButton1Click(Sender: TObject);
    procedure frm_empresabtn_agregarClick(Sender: TObject);
    procedure frm_Registrobtn_configurarClick(Sender: TObject);
    procedure frm_Registrobtn_desactivarClick(Sender: TObject);
    procedure MedidasClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

uses DataModule;


{$R *.fmx}
//Para encriptar contrase�as.
function md5(const Input: String): String;
var
  hCryptProvider: HCRYPTPROV;
  hHash: HCRYPTHASH;
  bHash: array[0..$7f] of Byte;
  dwHashBytes: Cardinal;
  pbContent: PByte;
  i: Integer;
begin
  dwHashBytes := 16;
  pbContent := Pointer(PChar(Input));
  Result := '';
  if CryptAcquireContext(@hCryptProvider, nil, nil, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT or CRYPT_MACHINE_KEYSET) then
  begin
    if CryptCreateHash(hCryptProvider, CALG_MD5, 0, 0, @hHash) then
    begin
      if CryptHashData(hHash, pbContent, Length(Input) * sizeof(Char), 0) then
      begin
        if CryptGetHashParam(hHash, HP_HASHVAL, @bHash[0], @dwHashBytes, 0) then
        begin
          for i := 0 to dwHashBytes - 1 do
          begin
            Result := Result + Format('%.2x', [bHash[i]]);
          end;
        end;
      end;
      CryptDestroyHash(hHash);
    end;
    CryptReleaseContext(hCryptProvider, 0);
  end;
  Result := AnsiLowerCase(Result);
end;



procedure TForm8.Art_listClick(Sender: TObject);
begin
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:= True;
  frm_articulo.Visible := True;
  frm_almacen.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_empresa.Visible := False;
  frm_cliente.Visible := False;
  frm_medidas.Visible := False;

  //Aqu� se ejecutan procesos internos del Frame
  frm_articulo.Visible := True;
end;

procedure TForm8.btn_formulariosClick(Sender: TObject);
begin
  if Configuracion_list.Visible = False then
  begin
    Configuracion_list.Visible := True;
  end
  else
  begin
    Configuracion_list.Visible := False;
  end;
end;

procedure TForm8.btn_proveedorClick(Sender: TObject);
begin
  {frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=True;
  Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  registro_usuarios1.Visible := False;
  Fam_indexprovedor1.cargartabla_proveedor;
  Index_CategoriaProductos1.Visible := False;
end;

procedure TForm8.btn_salirClick(Sender: TObject);
var
  selected: integer;
begin

  {
    Este es un ShowMessage de tipo YesNoQuestion el cual posee dos botones de s� y no
    En lugar de regresar TRUE o FALSE, dependiendo su respuesta recibira un n�mero.
    Para este caso si el valor de la variable <<selected> es igual a 6 la aplicaci�n
    cerrar�, de lo contrario continuar� normal

    @FUNCI�N
    La funci�n MessageDlg es similar al ShowMessage, pero este contendra botones asignados
    por nosotros y de utiliza de la siguiente manera:

      MessageDlg( mensaje, tipo de dialogo, [Arreglo de botones], 0);

    @NOTAS
    Todas los posibles tipos de dialogos, botones y respuestas se encuentran en el siguiente enlace.
    http://www.delphibasics.co.uk/RTL.asp?Name=messagedlg

    @PARAMS
    selected := Dato integer obtenido de la respuesta de ShowMessage(MessageDlg).
  }

  selected := MessageDlg('�Seguro que quieres salir?', TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0);
  if selected = 6 then
  begin
    Application.Terminate;
  end
  else
  begin

  end;

end;

procedure TForm8.Button1Click(Sender: TObject);
begin
  if frm_formularios.Visible = True then
  begin
  frm_formularios.Visible := false;
  frm_tablas.Visible := false;
  Frame_proveedor1.Visible := True;
  end
  else
  begin
  Frame_proveedor1.Visible := True;

  end;

end;

procedure TForm8.clientes_listClick(Sender: TObject);
begin
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:=False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_empresa.Visible := False;
  frm_medidas.Visible := False;

  //Aqu� se ejecutan procesos internos del Frame
  frm_cliente.Visible := True;
  frm_cliente.cargartabla;
end;

procedure TForm8.CornerButton1Click(Sender: TObject);
begin
//Categrias

Index_CategoriaProductos1.Visible := True;
Index_CategoriaProductos1.Reg_categoriasProducto1.Visible :=False;
//Para que el boton no se puede habilitar antes de que selecione un id
Index_CategoriaProductos1.btn_guardar.Enabled := True;
Index_CategoriaProductos1.btn_eliminar.Enabled := False;
Index_CategoriaProductos1.btn_modificar.Enabled := False;

end;

procedure TForm8.DivisasClick(Sender: TObject);
begin
  {frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:=False;
  frm_articulo.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_medidas.Visible := False;

  frm_almacen.Visible := false;}
  frm_div.Visible:=true;
end;

procedure TForm8.btn_clienteClick(Sender: TObject);
begin
  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  registro_usuarios1.Visible := False;

  frm_cliente.Visible := True;
  Index_CategoriaProductos1.Visible := False;
end;

procedure TForm8.Fam_indexprovedor1btn_agregarClick(Sender: TObject);
begin

//vas validaciones de los campos
Frame_proveedor1.Visible:= true;
frm_formularios.Visible := false;
frm_tablas.Visible := false;
fam_indexprovedor1.Visible := False;
Frame_proveedor1.cargardatos_estados;
//Frame_proveedor1.cargardatos_municipio;
Frame_proveedor1.cargar_empresa('123');
Frame_proveedor1.cargar_pais('123');
//Frame_proveedor1.cargar_municipio('123');
Frame_proveedor1.cargar_formapagos('123');

end;

procedure TForm8.FormActivate(Sender: TObject);
begin
//para mostrar los paises disponible
//conn.FDQuery_pais.Open();
end;

procedure TForm8.Frame_proveedor1btn_cancelarClick(Sender: TObject);
begin
Frame_proveedor1.Visible:= false;
frm_formularios.Visible := false;
frm_tablas.Visible := false;
fam_indexprovedor1.Visible := true;
Frame_proveedor1.limpiar;
end;

procedure TForm8.Frame_proveedor1btn_guardarClick(Sender: TObject);
begin
  Frame_proveedor1.btn_guardarClick(Sender);
   Fam_indexprovedor1.cargartabla_proveedor;
   Fam_indexprovedor1.Visible:= true;
   Frame_proveedor1.Visible:= false;
   Frame_proveedor1.limpiar;
end;

procedure TForm8.frm_empresabtn_agregarClick(Sender: TObject);
begin
  frm_empresa.btn_agregarClick(Sender);

end;

procedure TForm8.frm_loginCornerButton1Click(Sender: TObject);

{
Ejemplo para utilizar la encriptaci�n:
  procedure TForm1.Button1Click(Sender: TObject);
  begin
  Edit2.Text:= md5(Edit1.Text);
  end;
}

var
  username, pass, passCom: string;
begin

  {
   Las variables <<username>> y <<pass>>, seran asignadas los por TEdit
   contenidos en el Frame <<frm_login>> el cual corresponde al archivo Unit1.pas.

   @Notas
   La variable <<passCom>> por el momento no se utiliza, se har� cuando habilitemos
   la base de datos y se utilizar� para validar el inicio de sesi�n.

   @PARAMS
   username := Dato string obtenido de un TEdit.
   pass     := Dato string obtenido de un TEdit.
   pass     := Dato string obtenido como respuesta de la consulta SQL.
  }

  username:= frm_login.edt_username.Text;
  pass:= frm_login.edt_pass.Text;

  if (pass = '') or (username = '') then
  begin
    ShowMessage('�Los campos de usuario y contrase�a no pueden estar vacios!');
  end
  else
  begin
    frm_login.Visible:= False;
  end;

end;




procedure TForm8.frm_Registrobtn_configurarClick(Sender: TObject);
begin
  frm_empresa.frm_Registrobtn_configurarClick(Sender);

end;

procedure TForm8.frm_Registrobtn_desactivarClick(Sender: TObject);
begin
  frm_empresa.frm_Registrobtn_desactivarClick(Sender);

end;

procedure TForm8.ListBoxItem10Click(Sender: TObject);
begin

  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:=False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_empresa.Visible := False;

  frm_medidas.Visible := True;
  //Aqu� se ejecutan procesos internos del Frame
  frm_medidas.cargartabla;

end;

procedure TForm8.ListBoxItem1Click(Sender: TObject);
begin
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:=False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_medidas.Visible := False;

  frm_empresa.Visible := True;
  //Aqu� se ejecutan procesos internos del Frame
  frm_empresa.cargartabla;
end;

procedure TForm8.ListBoxItem2Click(Sender: TObject);
begin
  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_medidas.Visible := False;

  frm_sucursal.Visible := True;
  //Inicializamos la tabla
  frm_sucursal.cargartabla;
end;

procedure TForm8.ListBoxItem3Click(Sender: TObject);
begin
  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:=False;
  frm_articulo.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_medidas.Visible := False;

  frm_almacen.Visible := True;
end;

procedure TForm8.ListBoxItem4Click(Sender: TObject);
begin
  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_medidas.Visible := False;

  registro_usuarios1.Visible := True;
end;

procedure TForm8.ListBoxItem5Click(Sender: TObject);
begin
  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_Estados.Visible := False;
  frm_Municipio.Visible := False;
  frm_medidas.Visible := False;

  frm_paises.Visible := true;
  frm_paises.cargartabla;
end;

procedure TForm8.ListBoxItem6Click(Sender: TObject);
begin
  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Municipio.Visible := False;
  frm_medidas.Visible := False;

  frm_Estados.Visible := True;
  frm_Estados.cargartabla;
end;

procedure TForm8.ListBoxItem7Click(Sender: TObject);
begin

  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  registro_usuarios1.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_medidas.Visible := False;

  frm_Municipio.Visible := True;
  frm_Municipio.cargartabla;

end;

procedure TForm8.MedidasClick(Sender: TObject);
begin
       FrameMedidas1.Visible:=true;
end;

procedure TForm8.rec_btnClick(Sender: TObject);
var
  w: Single;
begin

  {
    La variable w se encarga de detectar el ancho del men� lateral en el momento
    de presionarlo, una vez sabiendolo comparar� si este esa abierto o cerrado
    en el caso de medir 100, significar� que este se encuentra abierto y realizar�
    la animaci�n para cerrarla (cerrar.rec), adem�s de ocultar los botones contenidos
    en el men�, si mide 0 har� justo lo contrar�o.

    @NOTAS
    El tipo de datos para obtener la altura y el ancho correspnde a <<Single>>.

    @PARAMS
    w := Dato tipo Single obtenido del ancho de la barra.

  }

  w:= left_rec.Width;
  if w = 100 then
  begin
    cerrar_rec.Start;
    btn_salir.Visible:= False;
  end
  else if w = 0 then
  begin
    abrir_rec.Start;
    btn_salir.Visible:= True;
  end;

end;



end.
