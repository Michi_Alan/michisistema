unit IndexPaises;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects, DataModule,
  FMX.Controls.Presentation, RegistroPais;

type
  TFrame_Paises = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    Grd_Paises: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    frm_registro: TFrame_RegPais;
    procedure cargartabla;
    procedure Grd_PaisesCellClick(const Column: TColumn; const Row: Integer);
    procedure btn_configurarClick(Sender: TObject);
    procedure Grd_PaisesCellDblClick(const Column: TColumn; const Row: Integer);
    procedure frm_registrobtn_configurarClick(Sender: TObject);
    procedure frm_registrobtn_desactivarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    var
      id: integer;
  end;

implementation

{$R *.fmx}

procedure TFrame_Paises.btn_configurarClick(Sender: TObject);
begin
  frm_registro.id := Self.id;
  frm_registro.cargardatos;
  frm_registro.Visible := True;
end;

procedure TFrame_Paises.cargartabla;
  var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.iso  FROM paises a;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Paises.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_Paises.Cells[0, I] := CONN.SQL_SELECT.FieldByName('iso').AsString;
          Grd_Paises.Cells[1, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;


  end;

procedure TFrame_Paises.frm_registrobtn_configurarClick(Sender: TObject);
begin
  frm_registro.Visible:= False;
  self.cargartabla;
end;

procedure TFrame_Paises.frm_registrobtn_desactivarClick(Sender: TObject);
var
iso, nombre: String;
begin

  iso := frm_registro.edt_iso.Text;
  nombre := frm_registro.edt_Nombre.Text;

  if (nombre = '') or (iso = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'UPDATE paises SET nombre= :nombre, iso= :iso WHERE id= :id';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('iso').AsString := iso;
    CONN.SQL_SELECT.ParamByName('id').AsInteger := frm_registro.id;

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      frm_Registro.Visible:= False;
      Self.cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;
  end;

end;

procedure TFrame_Paises.Grd_PaisesCellClick(const Column: TColumn;
  const Row: Integer);
begin
  id :=  Row+1;
  btn_configurar.Enabled := True;
end;


procedure TFrame_Paises.Grd_PaisesCellDblClick(const Column: TColumn;
  const Row: Integer);
begin
    id :=  Row+1;
  btn_configurar.Enabled := True;
end;

end.
