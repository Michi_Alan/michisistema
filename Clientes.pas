unit Clientes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Controls.Presentation, System.Rtti, FMX.Grid.Style, FMX.Grid,
  FMX.ScrollBox, FMX.Edit, RegistroAlm, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, Insert_cte_shido;
type
  TFrame_IndexCliente = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    Grd_Empresa: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    StringColumn7: TStringColumn;
    Frame_Cliente1: TFrame_Cliente;
    procedure btn_buscarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses Datamodule;


procedure TFrame_IndexCliente.btn_buscarClick(Sender: TObject);
begin
{ Datamodule.CONN.SQL_SELECT.Close;
  Datamodule.CONN.SQL_SELECT.SQL.Text := 'SELECT * ' +
                                          'FROM "clientes" ' +
                                           'WHERE id = ''' + edt_almbuscar.Text + '''' ;
  Datamodule.CONN.SQL_SELECT.Open;}

end;

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */
         /* ia stoi artooo alv*/
    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS ''ID_Cliente'',        '+
                                  'a.nombre AS ''Nom_Clientes'',       '+
                                  'f.Descripci�n AS ''CFDI'',          '+
                                  'a.RFC AS ''RFC'',                   '+
                                  'a.persona_tipo AS ''Tipo_Persona'', '+
                                  'a.telefono AS ''Telefono'',         '+
                                  'a.direccion AS ''Direccion'',       '+
                                  'a.colonia AS ''Colonia'',           '+
                                  'a.cp AS ''CP'',                     '+
                                  'b.nombre AS ''Pais'',               '+
                                  'c.nombre ''Estados'',               '+
                                  'd.nombre AS ''Municipios'',         '+
                                  'a.id_tipo AS ''Tipo'',              '+
                                  'a.id_empresa AS ''id_Empresa'',     '+
                                  'a.correo AS ''Correo'',             '+
                                  'e.nombre AS ''Empresa''             '+
                                  'FROM clientes a                     '+
                                  'JOIN paises b                       '+
                                  'ON a.id_pais = b.id                 '+
                                  'JOIN estados c                      '+
                                  'ON a.id_estado = c.id               '+
                                  'JOIN municipios d                   '+
                                  'ON a.id_municipio = d.id            '+
                                  'JOIN empresa e                      '+
                                  'ON a.id_empresa = e.id              '+
                                  'JOIN f4_c_usocfdi f                 '+
                                  'ON a.id_cfdi = f.id;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

       for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Empresa.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_Empresa.Cells[0, I] := CONN.SQL_SELECT.FieldByName('Nom_Clientes').AsString;
          Grd_Empresa.Cells[1, I] := CONN.SQL_SELECT.FieldByName('CDFI').AsString;
          Grd_Empresa.Cells[2, I] := CONN.SQL_SELECT.FieldByName('RFC').AsString;
          Grd_Empresa.Cells[3, I] := CONN.SQL_SELECT.FieldByName('Tipo_Persona').AsString;
          Grd_Empresa.Cells[4, I] := CONN.SQL_SELECT.FieldByName('Telefono').AsString;
          Grd_Empresa.Cells[5, I] := CONN.SQL_SELECT.FieldByName('Direccion').AsString;
          Grd_Empresa.Cells[6, I] := CONN.SQL_SELECT.FieldByName('Colonia').AsString;
          Grd_Empresa.Cells[7, I] := CONN.SQL_SELECT.FieldByName('CP').AsString;
          Grd_Empresa.Cells[8, I] := CONN.SQL_SELECT.FieldByName('Pais').AsString;
          Grd_Empresa.Cells[12, I] := CONN.SQL_SELECT.FieldByName('Estados').AsString;
          Grd_Empresa.Cells[13, I] := CONN.SQL_SELECT.FieldByName('Municipios').AsString;
          Grd_Empresa.Cells[14, I] := CONN.SQL_SELECT.FieldByName('Tipo').AsString;
          Grd_Empresa.Cells[15, I] := CONN.SQL_SELECT.FieldByName('id_Empresa').AsString;
          Grd_Empresa.Cells[16, I] := CONN.SQL_SELECT.FieldByName('Correo').AsString;
          Grd_Empresa.Cells[17, I] := CONN.SQL_SELECT.FieldByName('Empresa').AsString;
          CONN.SQL_SELECT.Next;

        end;

    end;


  end;

  procedure TFrame_IndexCliente.btn_agregarClick(Sender: TObject);
    begin
      frame_Cliente1.Visible := True;
      frame_Cliente1.cargardatos;
    end;


procedure TFrame_IndexCliente.btn_desactivarClick(Sender: TObject);
 var
  id_estado, id_municipio, id_pais, cfdi, tipo: integer;
  nombre, rfc, tel, direc, col, cp, correo: String;
 begin
     {
    Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
    si este fuera el caso podra la variable como vacia y no podr� pasar la
    siguiente condici�n.
  }

    id_estado := frame_Cliente1.cb_Estados.Selected.Index+1;
    id_municipio := frame_Cliente1.cb_Municipio.Selected.Index+1;
    id_pais := frame_Cliente1.cb_pais.Selected.Index+1;
    cfdi := frame_Cliente1.cb_cfdi.Selected.Index+1;
    tipo := frame_Cliente1.cb_tipo.Selected.Index+1;

    nombre := frame_Cliente1.edt_Nombre.Text;
    rfc := frame_Cliente1.edt_rfc.Text;
    tel := frame_Cliente1.edt_telefono.Text;
    direc := frame_Cliente1.edt_Direccion.Text;
    col :=  frame_Cliente1.edt_Colonia.text;
    cp := frame_Cliente1.edt_CP.Text;
    correo := frame_Cliente1.edt_correo.Text;

  ShowMessage(id_estado.ToString+', '+id_municipio.ToString+', '+id_pais.ToString);

  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_estado = 0 ) and (id_municipio = 0) and (id_pais = 0) and (cfdi = 0) and (tipo = 0)
   or (nombre = '')  or (rfc = '') or (tel = '') or (direc = '') or
      (col = '') or (cp = '') or (correo = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO empresa '+
    '(nombre, id_regimen, razon_social, RFC, telefono, direccion, colonia, CP, id_municipio, id_estado) '+
    'VALUES (:nombre, :regimen, :razon, :rfc, :tel, :direc, :col, :cp, :muni, :estado)';

    CONN.SQL_SELECT.ParamByName('Nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('RFC').AsString := rfc;
    CONN.SQL_SELECT.ParamByName('tipo').AsString := tipo.ToString;
    CONN.SQL_SELECT.ParamByName('Telefono').AsString := tel;
    CONN.SQL_SELECT.ParamByName('Direccion').AsString := direc;
    CONN.SQL_SELECT.ParamByName('Colonia').AsString := col;
    CONN.SQL_SELECT.ParamByName('CP').AsString := cp;
    CONN.SQL_SELECT.ParamByName('muni').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
    CONN.SQL_SELECT.ParamByName('pais').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('Persona_tipo').AsString := tel;
    CONN.SQL_SELECT.ParamByName('Correo').AsString := correo;
    CONN.SQL_SELECT.ParamByName('Empresa').AsString := cp;







    if (CONN.SQL_SELECT.RowsAffected > 0) then
      begin
        ShowMessage('�Se ha registrado la informaci�n!');
        frame_Cliente1.Visible:= False;
        cargartabla;
      end
    else
      begin
        ShowMessage('Algo ha salido mal...');
      end;

  end;
 end;

end.
