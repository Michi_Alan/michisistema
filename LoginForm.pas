unit LoginForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.Objects;

type
  TFrame1 = class(TFrame)
    rec_principal: TRectangle;
    rec_login: TRectangle;
    edt_pass: TEdit;
    edt_username: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    CornerButton1: TCornerButton;
    procedure edt_usernameKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TFrame1.edt_usernameKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if not(KeyChar in ['0' .. '9', 'a' .. 'z', 'A' .. 'Z', ' ', '-', #9, #8]) then
  begin
    KeyChar:= #0;
  end;
end;

end.
