unit Rutas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox,
  FMX.Controls.Presentation, FMX.Edit, FMX.DateTimeCtrls, FMX.Objects;

type
  TFrame4 = class(TFrame)
    rect_container: TRectangle;
    lbl_rutas: TText;
    Line1: TLine;
    pan_contenido: TRectangle;
    lbl_fecha: TText;
    DateEdit1: TDateEdit;
    lbl_observaciones: TText;
    edt_observaciones: TEdit;
    btn_Buscar: TCornerButton;
    Grid1: TGrid;
    Column1: TColumn;
    Column2: TColumn;
    Column3: TColumn;
    Column4: TColumn;
    Column5: TColumn;
    lbl_nuevo: TCornerButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

end.
