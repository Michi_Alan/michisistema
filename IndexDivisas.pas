unit IndexDivisas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.ListBox, FMX.Edit, FMX.Grid, FMX.ScrollBox,
  FMX.Objects, FMX.Controls.Presentation, DataModule;

type
  TFrameDivisas = class(TFrame)
    tab_cont: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    grd_div: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn4: TStringColumn;
    insert_cont: TRectangle;
    Label1: TLabel;
    Line2: TLine;
    Panel1: TPanel;
    Rectangle1: TRectangle;
    CornerButton1: TCornerButton;
    CornerButton2: TCornerButton;
    edt_Nombre: TEdit;
    lbs_Nombre: TLabel;
    edt_precio: TEdit;
    lbs_precio: TLabel;
    lbs_Pais: TLabel;
    cb_Pais: TComboBox;
    procedure CornerButton1Click(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
    procedure cargartabla;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TFrameDivisas.btn_agregarClick(Sender: TObject);
begin
  insert_cont.Visible:=true;
tab_cont.Visible:=false;
end;

procedure TframeDivisas.cargartabla;
         var I:integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'select a.id, a.divisa, b.nombre, a.precio_m from divisas a join paises b on b.id=a.id_pais ';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount -1 do
        begin
          Grd_div.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_div.Cells[0, I] := CONN.SQL_SELECT.FieldByName('divisa').AsString;
          Grd_div.Cells[1, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
          Grd_div.Cells[2, I] := CONN.SQL_SELECT.FieldByName('precio_m').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;


  end;


procedure TFrameDivisas.CornerButton1Click(Sender: TObject);
begin
insert_cont.Visible:=false;
tab_cont.Visible:=true;
end;

end.
