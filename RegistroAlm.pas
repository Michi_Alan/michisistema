unit RegistroAlm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation, FMX.ListBox;

type
  TFrame3 = class(TFrame)
    rec_container: TRectangle;
    pan_Registro: TPanel;
    lbs_Registro_Alm: TLabel;
    Line1: TLine;
    edt_Nombre: TEdit;
    lbs_Nombre: TLabel;
    edt_Direccion: TEdit;
    lbs_Direccion: TLabel;
    edt_Colonia: TEdit;
    lbs_Colonia: TLabel;
    lbs_Municipio: TLabel;
    lbs_Estado: TLabel;
    lbs_CP: TLabel;
    edt_CP: TEdit;
    cb_Estados: TComboBox;
    cb_Municipio: TComboBox;
    btn_Guardar: TCornerButton;
    btn_Cancelar: TCornerButton;
    Panel1: TPanel;


    function cargardatos(nombre: string): string;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_GuardarClick(Sender: TObject);
    procedure limpiardatos;
    procedure cb_EstadosClosePopup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses DataModule;

{$R *.fmx}

procedure TFrame3.limpiardatos;
begin
  edt_Nombre.Text := '';
  edt_Direccion.Text :=  '';
  edt_Colonia.Text := '';
  edt_CP.Text := '';
  cb_Estados.Items.Clear;
  cb_Municipio.Items.Clear;
end;

procedure TFrame3.btn_CancelarClick(Sender: TObject);
begin
self.Visible := False;
end;

procedure TFrame3.btn_GuardarClick(Sender: TObject);

var
Nombre, Direccion, Colonia, Municipio, Estado, CP, Pais: String;
begin

  Nombre:= edt_Nombre.Text;
  Direccion := edt_Direccion.Text;
  Colonia := edt_Colonia.Text;
  CP := edt_CP.Text;

  if (Nombre = '') or (Direccion = '') or (Colonia = '') or (Municipio = '') or (Estado = '') or (CP = '') then
    begin
      ShowMessage('�No puedes dejar informaci�n vacia!');
    end
  else
    begin
            CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'INSERT `almacenes` SET `nombre`=:Nombre,`direccion`=:Direccion,`colonia`=:Colonia';

            CONN.SQL_SELECT.ParamByName('Nombre').AsString := Nombre;
            CONN.SQL_SELECT.ParamByName('Direccion').AsString := Direccion;
            CONN.SQL_SELECT.ParamByName('Colonia').AsString := Colonia;
            CONN.SQL_SELECT.ParamByName('CP').AsString := CP;
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then
              begin
                ShowMessage('�Se han actualizado los datos!');
              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;
    end;
end;

function TFrame3.cargardatos(nombre: string): string;
begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM estados WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Estados.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Estados.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  //

  //

end;

procedure TFrame3.cb_EstadosClosePopup(Sender: TObject);

begin
  cb_Municipio.Items.Clear;
  cb_Municipio.Enabled := True;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

  CONN.SQL_SELECT.ParamByName('id').AsInteger := cb_Estados.Selected.Index+1;
  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_Municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
end;

end.
